import React, { useEffect} from 'react';

const Form=()=>{
    const field=React.useRef<any>();
    useEffect(()=>{
        field.current.focus();
    })

    return(
        <div> 
            <br/>
            <br/>      
            <label className ="labelfield" htmlFor="fname" ref={field}>First Name:</label>
            <input className="input1" id="fname" name="fname" type="text" />
            <br/>

            <label className ="labelfield" htmlFor="lname">Last Name:</label>
            <input className="input2" id="lname" name="lname" type="text" />
            <br/>
            
            <label className ="labelfield" htmlFor="email">Email:</label>
            <input className="input3" id="email" name="email" type="email" />
            <br/>
            
            <label className ="labelfield" htmlFor="phone">Contact Number:</label>
            <input className="input4" id="phone" name="phone" type="number" />
            <br/>

            <button className="inputbutton" type="button">Submit</button>
        </div>
    )
}
export default Form;