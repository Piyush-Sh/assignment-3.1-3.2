
import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import Form from './Form';
import "./Index.css";

const Counter=()=>{
    const [count,updatecount]= useState<number>(Number(localStorage.getItem('count')) || 0);
    
    
    useEffect(()=>{
        updatecount(count=>count+1);
        
    },[])
    
    useEffect(()=>{
        console.log("Working Use Effect");
        localStorage.setItem('count',String(count));
    },[count]);

    const decrement=():void=> {
        if (count === 0) {
          window.confirm("You cannot order negative quantity!");
        }
        else{
        updatecount((count) => count - 1);
      }}
    
    const reset=():void=>{
        const msg:string="you want to set 0";
        if(window.confirm(msg)){
            updatecount(0)
        }
    }

    return(
        <div className="maindiv">
            <div className="countdiv">
            <h1 className="head">Count is {count}</h1>
            
            <button className="button" onClick={()=>updatecount(count+1)}>Increase</button>
            <button className="button" onClick={()=>reset()}>RESET</button>
            <button className="button" onClick={decrement}>Decrease</button>
            </div>
            <div className="formdiv">
            <Form/>
            </div>
        </div>
    )
};

ReactDOM.render(<Counter />,document.querySelector('#root'));

